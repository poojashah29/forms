import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MededucationComponent } from './mededucation.component';

describe('MededucationComponent', () => {
  let component: MededucationComponent;
  let fixture: ComponentFixture<MededucationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MededucationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MededucationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
