import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelofinfComponent } from './relofinf.component';

describe('RelofinfComponent', () => {
  let component: RelofinfComponent;
  let fixture: ComponentFixture<RelofinfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelofinfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelofinfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
