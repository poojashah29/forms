import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstverfComponent } from './instverf.component';

describe('InstverfComponent', () => {
  let component: InstverfComponent;
  let fixture: ComponentFixture<InstverfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstverfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstverfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
