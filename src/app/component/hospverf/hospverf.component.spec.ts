import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HospverfComponent } from './hospverf.component';

describe('HospverfComponent', () => {
  let component: HospverfComponent;
  let fixture: ComponentFixture<HospverfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HospverfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HospverfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
