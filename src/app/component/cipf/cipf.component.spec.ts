import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CipfComponent } from './cipf.component';

describe('CipfComponent', () => {
  let component: CipfComponent;
  let fixture: ComponentFixture<CipfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CipfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CipfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
