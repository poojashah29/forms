import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracverfComponent } from './pracverf.component';

describe('PracverfComponent', () => {
  let component: PracverfComponent;
  let fixture: ComponentFixture<PracverfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracverfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracverfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
