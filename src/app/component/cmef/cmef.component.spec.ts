import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmefComponent } from './cmef.component';

describe('CmefComponent', () => {
  let component: CmefComponent;
  let fixture: ComponentFixture<CmefComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
