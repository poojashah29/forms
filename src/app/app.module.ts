import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './component/footer/footer.component';
import { HeaderComponent } from './component/header/header.component';
import { TempComponent } from './component/temp/temp.component';
import { GeneralinformationComponent } from './component/generalinformation/generalinformation.component';
import { RelofinfComponent } from './component/relofinf/relofinf.component';
import { QuestionComponent } from './component/question/question.component';
import { PaymentComponent } from './component/payment/payment.component';
import { MededucationComponent } from './component/mededucation/mededucation.component';
import { HospverfComponent } from './component/hospverf/hospverf.component';
import { InstverfComponent } from './component/instverf/instverf.component';
import { CmeComponent } from './component/cme/cme.component';
import { CmefComponent } from './component/cmef/cmef.component';
import { CipfComponent } from './component/cipf/cipf.component';
import { PracverfComponent } from './component/pracverf/pracverf.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    TempComponent,
    GeneralinformationComponent,
    RelofinfComponent,
    QuestionComponent,
    PaymentComponent,
    MededucationComponent,
    HospverfComponent,
    InstverfComponent,
    CmeComponent,
    CmefComponent,
    CipfComponent,
    PracverfComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
